from setuptools import setup, find_packages

setup(name='KDE-quality-check',
      version="0.0.1",
      packages=find_packages(
          exclude=["kuality"]),
      install_requires=[
      ],
      author="Carl Schwan",
      author_email="carl@carlschwan.eu",
      entry_points={
          "console_scripts": [
              'kuality = kuality.cli:main',
          ]
      },
      python_requires='>=3.6')
