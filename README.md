# A collection of tools used for static analysis for KDE projects

## Installation

```
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
pip3 install --upgrade -e .
```

## Usage

Example: Run AppStreamValidator on a krita repository

```
mkdir projects
git clone git@invent.kde.org:kde/krita.git projects/krita
kuality -n krita -p projects/krita -t AppStreamValidateTool
```

Example Run CppCheck on a kwayland repository

```
mkdir projects
git clone kde:kwayland projects/kwayland
kuality -n kwayladn -p projects/kwayland -t CppCheckTool
```


## License

This project is released under GPL-3.0-or-later
