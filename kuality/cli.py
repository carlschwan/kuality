import typing as tp
from kuality.project import Project
from kuality.tools.tool import Tool 
from kuality.tools.tools import ToolRegistry
import argparse
import logging
from pathlib import Path
import os

LOG = logging.getLogger(__name__)

def main() -> None:
    parser = argparse.ArgumentParser("kuality")
    parser.add_argument("-n", "--name", help="Project name")
    parser.add_argument("-p", "--path", help="Project path")
    parser.add_argument("-t", "--tools", nargs='+', help="Analytics tools used: " + ToolRegistry.get_tool_types_help_string())
    
    args = {k: v for k, v in vars(parser.parse_args()).items() if v is not None}

    __run(args)


def __run(args: tp.Dict[str, tp.Any]) -> None:
    project = Project(args['name'], os.path.abspath(args['path']))

    for tool_type in args['tools']:
        tool_t = ToolRegistry.get_class_for_tool_type(tool_type)
        t = tool_t()
        errors = t.run(project)
        print('\n'.join('{}: {}'.format(*k) for k in enumerate(errors)))



if __name__ == '__main__':
    main()






