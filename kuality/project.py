import typing as tp
from pathlib import Path
import abc

class Project():
    """A project to be tested"""

    def __init__(self, name: str, path: Path, **kwargs: tp.Any) -> None:
        self.__name = name
        self.__path = path
        self.__saved_extra_args = kwargs

    @property
    def name(self) -> str:
        """
        Name of the current tool.
 
        Test:
        >>> Tool('test').name
        'test'
        """
        return self.__name

    @property
    def path(self) -> str:
        """
        Path of the current project.
 
        Test:
        >>> Project('testpath').name
        'testpath'
        """
        return self.__path

    @property
    def project_kwargs(self) -> tp.Any:
        """
        Access the kwargs passed to the initial project.

        Test:
        >>> p = Project('test', foo='bar', baz='bazzer')
        >>> p.project_kwargs['foo']
        'bar'
        >>> p.project_kwargs['baz']
        'bazzer'
        """
        return self.__saved_extra_args

