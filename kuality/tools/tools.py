import re
import sys
import typing as tp

from typing import TYPE_CHECKING
if TYPE_CHECKING:
    import kuality.tools.tool as tool

class ToolRegistry(type):
    """
    Registry for all supported tools.
    """

    to_snake_case_pattern = re.compile(r'(?<!^)(?=[A-Z])')

    tools: tp.Dict[str, tp.Type[tp.Any]] = {}

    def __init__(cls, name: str, bases: tp.Tuple[tp.Any],
                 attrs: tp.Dict[tp.Any, tp.Any]):
        super(ToolRegistry, cls).__init__(name, bases, attrs)
        if hasattr(cls, 'NAME'):
            key = getattr(cls, 'NAME')
        else:
            key = ToolRegistry.to_snake_case_pattern.sub('_', name).lower()
        ToolRegistry.tools[key] = cls

    @staticmethod
    def get_tool_types_help_string() -> str:
        """
        Generates help string for visualizing all available tools.

        Returns:
            a help string that contains all available tools names.
        """
        return "The following tools are available:\n  " + "\n  ".join(
            [key for key in ToolRegistry.tools if key != "tool"])

    @staticmethod
    def get_class_for_tool_type(tool: str) -> tp.Type['tools.Tool']:
        """
        Get the class for tool from the tool registry.

        Test:
        >>> ToolRegistry.get_class_for_tool_type('app_stream_tool')
        <class 'kuality.tools.appstream.AppStreamTool'>

        Args:
            tool: The name of the tool.

        Returns: The class implementing the tool.
        """
        from kuality.tools.tool import Tool
        if tool not in ToolRegistry.tools:
            sys.exit(f"Unknown tool '{tol}'.\n" +
                     ToolRegistry.get_tool_types_help_string())

        tool_cls = ToolRegistry.tools[tool]
        if not issubclass(tool_cls, Tool):
            raise AssertionError()
        return tool_cls

