import typing as tp
import abc
from kuality.project import Project
from pathlib import Path

from kuality.tools.tools import ToolRegistry


class Tool(metaclass=ToolRegistry):
    """An abstract analytics tool"""

    def __init__(self, **kwargs: tp.Any) -> None:
        self.__saved_extra_args = kwargs


    @property
    def tool_kwargs(self) -> tp.Any:
        """
        Access the kwargs passed to the initial tool.

        Test:
        >>> t = Tool('test', foo='bar', baz='bazzer')
        >>> r.tool_kwargs['foo']
        'bar'
        >>> t.tool_kwargs['baz']
        'bazzer'
        """
        return self.__saved_extra_args

    @abc.abstractmethod
    def run(self, project: Project) -> None:
        """Run the current tool on the """
