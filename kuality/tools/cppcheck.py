import abc
import os
import typing as tp
from plumbum import local
from kuality.project import Project
from kuality.tools.tool import Tool 
from pathlib import Path
from tempfile import TemporaryDirectory
from kuality.results import Results, Error
import xml.etree.ElementTree as ET

class CppCheckTool(Tool):
    NAME = "CppCheckTool"

    def run(self, project: Project) -> Results:
        """Run appstreamcli validate"""

        results = Results()

        with TemporaryDirectory(prefix="kuality_" + project.name + "_") as tmp_dir:
            with local.cwd(tmp_dir):
                cmake = local['cmake']
                cmake['-DCMAKE_EXPORT_COMPILE_COMMANDS=ON', project.path]()
                cppcheck = local['cppcheck']
                result = cppcheck['--project=compile_commands.json', '--xml', '--library=qt', '-q'].run()
                root = ET.fromstring(result[2])
                for error_node in root.iter('error'):
                    error = Error(error_node.attrib['severity'], error_node.attrib['msg'], error_node.attrib['verbose'])
                    results.append(error)

        return results

            


                

