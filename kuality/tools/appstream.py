import abc
import os
import typing as tp
from plumbum import local
from kuality.project import Project
from kuality.tools.tool import Tool 
from pathlib import Path

class AppStreamTool(Tool):
    NAME = "AppStreamValidateTool"

    @staticmethod
    def __find_appstream_files(project_path: str) -> tp.Set[Path]:
        appstream_files = set()
        for (dirpath, dirnames, filenames) in os.walk(project_path):
            for f in filenames:
                if f.endswith('.appdata.xml'):
                    appstream_files.add(dirpath + "/" + f)
        return appstream_files


    def run(self, project: Project) -> None:
        """Run appstreamcli validate"""

        appstreamcli = local['appstreamcli']
        appstream_files = AppStreamTool.__find_appstream_files(project.path)
        if len(appstream_files) == 0:
            print("No AppStream file found")

        for file in appstream_files:        
            result = appstreamcli['validate', file]()
            print(result)
