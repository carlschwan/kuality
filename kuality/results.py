import typing as tp

class Error():

    def __init__(self, severity: str, msg: str, verbose: str, inconclusive=False):
        self.__severity = severity
        self.__msg = msg
        self.__verbose = verbose
        self.inconclusive = inconclusive

    def __str__(self):
        return "{} {}".format(self.__severity, self.__msg)


class Results(list):
    
    def __init__(self, *args):
        list.__init__(self, *args)

